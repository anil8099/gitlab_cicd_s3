terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.31.0"
    }
  }
  backend "s3" {
    bucket = "anil8099-gitlab-cicd-backend"
    key    = "cicd/terraform.tfstate"  #name of the S3 object that will store the state file
    region = "us-east-1"
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "ap-south-1"
}
